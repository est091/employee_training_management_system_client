export default async function googleApi() {

	const sheetData = await getServerSideProps();
	return { data: sheetData.props.sheetdata }
}


export async function getServerSideProps() {

	const req = await fetch('http://localhost:3000/api/sheet');
	const res = await req.json();
	return { props: { sheetdata: res.data } }
  
}