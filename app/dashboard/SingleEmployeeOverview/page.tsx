import googleApi from '../../../pages/googleApi';
import EmployeeSelection from '../../ui/dashboard/EmployeeSelection';

export default async function Page() {
	const { data } = await googleApi();
	const sheetdata = Object.assign({}, [...data.values]);
	
	const sheetdataObject = {};
	const sheetdataObjectKeys = {};
	const finalSheetdataObject = {};

	Object.keys(sheetdata).forEach((key: any) => {

		if(key === "1")
		{
			const eachKey = sheetdata[key].reduce((keys: any, value: any) => ({ ...keys, [value]: "" }), {});
			Object.assign(sheetdataObjectKeys, eachKey);
		}
		if(key > "1")
		{

			const eachValue = sheetdata[key].reduce((keys: any, value: any, index: number) => ({ ...keys, [index]: value }), {});
			Object.assign(sheetdataObject, eachValue);

			Object.keys(sheetdataObjectKeys).forEach((keys: any, index: number) => {
				sheetdataObjectKeys[keys] = (sheetdataObject[keys] !== '') ? sheetdataObject[index] : '' ;
			})
		}
		Object.assign(finalSheetdataObject, { [key]: {...sheetdataObjectKeys} });
	});

	return (
		<>
			<div>
				<EmployeeSelection rowData = { finalSheetdataObject } />
			</div>
		</>
	);
}
