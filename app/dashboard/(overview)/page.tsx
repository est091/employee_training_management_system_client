import { lusitana } from '@/app/ui/fonts';
import googleApi from '../../../pages/googleApi';
import { GanttChart } from '@/app/ui/dashboard/GanttChart';
 
export default async function Page() {

	const sheetdataObject = {};
	const sheetdataObjectKeys = {};
	const finalSheetdataObject = {};
	const { data } = await googleApi();
	const sheetdata = Object.assign({}, [...data.values]);
	

	for(let item in sheetdata)
	{
		if(item === "1")
		{
			const eachKey = sheetdata[item].reduce((keys: any, value: any) => ({ ...keys, [value]: "" }), {});
			Object.assign(sheetdataObjectKeys, eachKey);
		}

		const eachValue = sheetdata[item].reduce((keys: any, value: any, index: number) => ({ ...keys, [index]: value }), {});
		Object.assign(sheetdataObject, eachValue);

		Object.keys(sheetdataObjectKeys).forEach((keys: any, index: number) => {
			sheetdataObjectKeys[keys] = (sheetdataObject[keys] !== '') ? sheetdataObject[index] : '' ;
		})

		item === '128' ? Object.assign(finalSheetdataObject, { 0: {...sheetdataObjectKeys}}) : item === '160' ? Object.assign(finalSheetdataObject, { 1: {...sheetdataObjectKeys}}) : null;
	}

	return (
		<main>
			<h1 className={`${lusitana.className} mb-4 text-xl md:text-2xl text-center`}>
				Dashboard
			</h1>
			<div className='w-full h-full'>
				<GanttChart rowData = { finalSheetdataObject } />
			</div>
		</main>
	);
}