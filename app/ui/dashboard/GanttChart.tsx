'use client';

import * as React from 'react';
import Badge from '@mui/material/Badge';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import Switch from '@mui/material/Switch';
import Grid from '@mui/material/Unstable_Grid2';
import FormControlLabel from '@mui/material/FormControlLabel';


function createData(Emp_Code: string, Emp_Name: string, Designation: string, Active_Inactive: string, Department: string, Training_Topic: string, Logic_Building: string, JavaScript_ES6: string, Git_Client: string, HTML: string, CSS: string, Bootstrap: string, SQL: string, HTTP_Protocols: string, NoSql_Database: string, couchBase: string, Angular: string, Rust: string, Node_JS: string, Android: string, IOS: string, CSharp: string, Kafka: string, Gherkin: string, Git: string, Linux: string, Docker: string, Kubernetes: string, Total_Training_Hrs: string ) {
	return {
		Emp_Code,
		Emp_Name,
		Designation,
		Active_Inactive,
		Department,
		Training_Topic: [
			{ Logic_Building: Logic_Building},
			{ JavaScript_ES6: JavaScript_ES6},
			{ Git_Client: Git_Client},
			{ HTML: HTML},
			{ CSS: CSS},
			{ Bootstrap: Bootstrap},
			{ SQL: SQL},
			{ HTTP_Protocols: HTTP_Protocols},
			{ NoSql_Database: NoSql_Database},
			{ couchBase: couchBase},
			{ Angular: Angular},
			{ Rust: Rust},
			{ Node_JS: Node_JS},
			{ Android: Android},
			{ IOS: IOS},
			{ CSharp: CSharp},
			{ Kafka: Kafka},
			{ Gherkin: Gherkin},
			{ Git: Git},
			{ Linux: Linux},
			{ Docker: Docker},
			{ Kubernetes: Kubernetes},
		],
		Total_Training_Hrs,
	};
}

// Gantt Chart According to employees
export const GanttChart = ({ rowData }: any) => {

	const rows: any = [];
	const [invisible, setInvisible] = React.useState(true);

	Object.keys(rowData).forEach((keys: any) => {
		if(rowData[keys]['Emp Code'] !== undefined)
			if(rowData[keys]['Emp Code'] !== '')
				rows.push(createData(rowData[keys]['Emp Code'], rowData[keys]['Emp Name'], rowData[keys]['Designation'], rowData[keys]['Active/Inactive'], rowData[keys]['Department'], rowData[keys]['Training Topic'], rowData[keys]['Logic Building'], rowData[keys]['JavaScript / ES6'], rowData[keys]['Git Client'], rowData[keys]['HTML'], rowData[keys]['CSS'], rowData[keys]['Bootstrap'], rowData[keys]['SQL'], rowData[keys]['HTTP Protocols'], rowData[keys]['NoSql Database'], rowData[keys]['couchBase'], rowData[keys]['Angular'], rowData[keys]['Rust'], rowData[keys]['Node JS'], rowData[keys]['Android'], rowData[keys]['IOS'], rowData[keys]['CSharp'], rowData[keys]['Kafka'], rowData[keys]['Gherkin'], rowData[keys]['Git'], rowData[keys]['Linux'], rowData[keys]['Docker'], rowData[keys]['Kubernetes'], rowData[keys]['Total Training Hrs']));
	});

	const handleBadgeVisibility = () => setInvisible(!invisible);

	return (
		<>
			<FormControlLabel sx={{ color: 'text.default' }} control={<Switch checked={!invisible} onChange={handleBadgeVisibility} />} label="Show Details Of Trainer" />
			<Grid container>
				{
					rows.map((eachTrainer: any, index: number) => 
						<Grid xs key={index}>
							<Stack spacing={1} alignItems="center">
								<Stack direction="row" spacing={1}>
									<Badge color="error" badgeContent={ eachTrainer.Department } anchorOrigin={{ vertical: 'top', horizontal: 'right' }} invisible={invisible}>
										<Badge color="warning" badgeContent={ eachTrainer.Designation } anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} invisible={invisible}>
											<Badge color="info" badgeContent={ eachTrainer.Emp_Code } anchorOrigin={{ vertical: 'top', horizontal: 'left' }} invisible={invisible}>
												<Badge color="success" badgeContent={ eachTrainer.Active_Inactive } anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }} invisible={invisible}>
													<Paper className='p-5 bg-[#2D333A] text-white'>{ eachTrainer.Emp_Name }</Paper>
												</Badge>
											</Badge>
										</Badge>
									</Badge>
								</Stack>
							</Stack>
						</Grid>
					)
				}
			</Grid>
		</>
	)
}