'use client';

import * as React from 'react';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Table from '@mui/material/Table';
import Paper from '@mui/material/Paper';
import Collapse from '@mui/material/Collapse';
import TableRow from '@mui/material/TableRow';
import Grid from '@mui/material/Unstable_Grid2';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { BarChart } from '@mui/x-charts/BarChart';
import TableContainer from '@mui/material/TableContainer';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';



function createData(Emp_Code: string, Emp_Name: string, Designation: string, Active_Inactive: string, Department: string, Training_Topic: string, Logic_Building: string, JavaScript_ES6: string, Git_Client: string, HTML: string, CSS: string, Bootstrap: string, SQL: string, HTTP_Protocols: string, NoSql_Database: string, couchBase: string, Angular: string, Rust: string, Node_JS: string, Android: string, IOS: string, CSharp: string, Kafka: string, Gherkin: string, Git: string, Linux: string, Docker: string, Kubernetes: string, Total_Training_Hrs: string ) { return { Emp_Code, Emp_Name, Designation, Active_Inactive, Department, Training_Topic: [{ Logic_Building: Logic_Building}, { JavaScript_ES6: JavaScript_ES6}, { Git_Client: Git_Client}, { HTML: HTML}, { CSS: CSS}, { Bootstrap: Bootstrap}, { SQL: SQL}, { HTTP_Protocols: HTTP_Protocols}, { NoSql_Database: NoSql_Database}, { couchBase: couchBase}, { Angular: Angular}, { Rust: Rust}, { Node_JS: Node_JS}, { Android: Android}, { IOS: IOS}, { CSharp: CSharp}, { Kafka: Kafka}, { Gherkin: Gherkin}, { Git: Git}, { Linux: Linux}, { Docker: Docker}, { Kubernetes: Kubernetes}], Total_Training_Hrs}; }
  
function Row(props: { row: ReturnType<typeof createData> }) {
	const { row } = props;
	const [open, setOpen] = React.useState(false);

	const visibility = row.Total_Training_Hrs === "0" || row.Total_Training_Hrs === undefined || row.Active_Inactive === 'Left' ? 'hidden invisible' : ''

	const backgroundColors = 
	{
		Logic_Building: { backgroundColor:'#4169E1', color: '#FFFFFF' },    
		JavaScript_ES6: { backgroundColor:'#323330', color: '#FFFFFF' },    
		Git_Client: { backgroundColor:'#f0db4f', color: '#2D333A' },    
		HTML: { backgroundColor:'#e34c26', color: '#FFFFFF' },    
		CSS: { backgroundColor:'#2965f1', color: '#FFFFFF' },    
		Bootstrap: { backgroundColor:'#563d7c', color: '#FFFFFF' },    
		SQL: { backgroundColor:'#F29111', color: '#2D333A' },    
		HTTP_Protocols: { backgroundColor:'#F08080', color: '#FFFFFF' },    
		NoSql_Database: { backgroundColor:'#E8E7D5', color: '#2D333A' },    
		couchBase: { backgroundColor:'#fb1c25', color: '#FFFFFF' },    
		Angular: { backgroundColor:'#dd1b16', color: '#FFFFFF' },    
		Rust: { backgroundColor:'#b7410e', color: '#FFFFFF' },    
		Node_JS: { backgroundColor:'#3C873A', color: '#FFFFFF' },    
		Android: { backgroundColor:'#3DDC84', color: '#2D333A' },    
		IOS: { backgroundColor:'#ff2d55', color: '#FFFFFF' },    
		CSharp: { backgroundColor:'#A179DC', color: '#FFFFFF' },    
		Kafka: { backgroundColor:'#4B0B52', color: '#FFFFFF' },    
		Gherkin: { backgroundColor:'#67ab05', color: '#FFFFFF' },    
		Git: { backgroundColor:'#f34f29', color: '#FFFFFF' },    
		Linux: { backgroundColor:'#ffcc33', color: '#2D333A' },    
		Docker: { backgroundColor:'#0db7ed', color: '#2D333A' },    
		Kubernetes: { backgroundColor:'#326CE5', color: '#FFFFFF' },
	};

	let arr: any = [{}];

	row.Training_Topic.map((historyRow: any, index: number) => {
		if(historyRow[Object.keys(historyRow)[0]] !== 0 && historyRow[Object.keys(historyRow)[0]] !== "0" && historyRow[Object.keys(historyRow)[0]] !== undefined)
			arr.push({ data: Number(historyRow[Object.keys(historyRow)[0]]), label: `${Object.keys(historyRow)[0]}`, backgroundColor:`${backgroundColors[Object.keys(historyRow)[0] as keyof typeof backgroundColors].backgroundColor}` })
	});

	arr = arr.filter((value: any) => Object.keys(value).length !== 0);

	const chartSetting = { xAxis: [ { label: 'Training Attended' } ], width: 500, height: 250 };

	const valueFormatter = (value: number | null) => `${value}`;

	return (
		<>
			<TableRow className={`${visibility}`}>
				<TableCell>
					<IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}> { open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon /> } </IconButton>
				</TableCell>
				<TableCell component="th" scope="row"> { row.Emp_Code } </TableCell>
				<TableCell align="center"> { row.Emp_Name } </TableCell>
				<TableCell align="center"> { row.Designation } </TableCell>
				<TableCell align="center"> { row.Department } </TableCell>
				<TableCell align="center"> { row.Total_Training_Hrs } </TableCell>
				<TableCell align="center"> <Chip label={ row.Active_Inactive } color={`${row.Active_Inactive === 'Left' ? 'error' : 'success'}`} /> </TableCell>
			</TableRow>
			<TableRow>
				<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6} className={`${visibility}`}>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<Box sx={{ margin: 1 }}>
							<Typography variant="h6" gutterBottom component="div"> Total Training Hours: { row.Total_Training_Hrs }</Typography>
							<Box sx={{ flexGrow: 1 }}>
								<Grid container spacing={2}>
									<Grid xs={6}>
										<Grid container spacing={2}>
											{
												row.Training_Topic.map((historyRow: any, index: number) => (
													<>
														<Grid className={`${historyRow[Object.keys(historyRow)[0]] === undefined || historyRow[Object.keys(historyRow)[0]] === "0" ? "hidden invisible" : '' }`} key={index} xs={2} sm={3} md={3}>
															<Chip label={`${[Object.keys(historyRow)[0]]}: ${ historyRow[Object.keys(historyRow)[0]] }`} style={{backgroundColor:`${backgroundColors[Object.keys(historyRow)[0] as keyof typeof backgroundColors].backgroundColor}`, color: `${backgroundColors[Object.keys(historyRow)[0] as keyof typeof backgroundColors].color}`}} />
														</Grid>
													</>
												))
											}
										</Grid>
									</Grid>
									<Grid xs={6} className={`flex justify-center items-center self-center`}>
										<BarChart dataset={arr} yAxis={[{ scaleType: 'band', dataKey: 'label' }]} series= {[{dataKey: 'data', label: 'Hours Of Training', valueFormatter }]} layout="horizontal" {...chartSetting} />
									</Grid>
								</Grid>
							</Box>
						</Box>
					</Collapse>
				</TableCell>
			</TableRow>
		</>
	);
}

export default function DropDownTable({ rowData }: any) {

	const rows: any = [];

	Object.keys(rowData).forEach((keys: any) => {
		if(rowData[keys]['Emp Code'] !== undefined)
			if(rowData[keys]['Emp Code'] !== '')
				rows.push(createData(rowData[keys]['Emp Code'], rowData[keys]['Emp Name'], rowData[keys]['Designation'], rowData[keys]['Active/Inactive'], rowData[keys]['Department'], rowData[keys]['Training Topic'], rowData[keys]['Logic Building'], rowData[keys]['JavaScript / ES6'], rowData[keys]['Git Client'], rowData[keys]['HTML'], rowData[keys]['CSS'], rowData[keys]['Bootstrap'], rowData[keys]['SQL'], rowData[keys]['HTTP Protocols'], rowData[keys]['NoSql Database'], rowData[keys]['couchBase'], rowData[keys]['Angular'], rowData[keys]['Rust'], rowData[keys]['Node JS'], rowData[keys]['Android'], rowData[keys]['IOS'], rowData[keys]['CSharp'], rowData[keys]['Kafka'], rowData[keys]['Gherkin'], rowData[keys]['Git'], rowData[keys]['Linux'], rowData[keys]['Docker'], rowData[keys]['Kubernetes'], rowData[keys]['Total Training Hrs']));
	});

	return (
		<TableContainer component={Paper} sx={{ maxHeight: '89vh' }}>
			<Table aria-label="collapsible table a dense table" size="small">
				<TableHead>
					<TableRow>
						<TableCell />
						<TableCell>Employee Code</TableCell>
						<TableCell align="center">Employee Name</TableCell>
						<TableCell align="center">Designation</TableCell>
						<TableCell align="center">Department</TableCell>
						<TableCell align="center">Total Training Hours</TableCell>
						<TableCell align="center">Working / Left</TableCell>
					</TableRow>
				</TableHead>
				<TableBody> { rows.map((row: any, index: number) => ( <Row key={index} row={row} /> )) } </TableBody>
			</Table>
	  </TableContainer>
	);
}